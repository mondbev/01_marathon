#!/usr/bin/env bash

getopts_init()
{
case $1 in
  '')
    echo "Hello starnger" 1>&2
    ;;
  -h | --help)
    cat <<\EOF
NAME
        laboss - get lab information

SYNOPSIS
	laboss {-n|--name} 
	laboss {-h|--help} | {-v|--version}

OPTIONS
  	-n, --name <NAME>   
  	  	echo Hello NAME

EOF
    ;;
  -v | --version)
    echo "laboss 1.0.0"
    ;;
  -n | --name)
    echo "Hello $2"
    ;;
  *)
    echo "Hello stranger"
   ;;
esac
}
getopts_init "$@"
